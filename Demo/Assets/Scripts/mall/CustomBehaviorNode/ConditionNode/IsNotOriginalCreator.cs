﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsNotOriginalCreator : ConditionNode
{

    public GameObjectVar gameObject;

    // Called when the node will be ticked
    public override void OnTick()
    {
        GameObject unityGameObj = (GameObject)gameObject;
        Area correspondingArea = unityGameObj.GetComponent<Area>();

        status = Status.Failure;

        if (correspondingArea != null)
        {
            ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>();
            if (shopperEntity && shopperEntity != correspondingArea.originalCreator)
                status = Status.Success;
        }
    }
}
