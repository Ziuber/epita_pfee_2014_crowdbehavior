﻿
using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class SearchForTarget : ConditionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        float cosAngle = Mathf.Cos(Mathf.PI / 4);
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;
        Vector3 normalizedShopperVector = Vector3.Normalize(shopperEntity.navigationEntity.velocity);

        List<ShopperGlobal> agentInNodeList = shopperEntity.navigationEntity.currentNode.agentInNode;

        foreach (ShopperGlobal testedShopper in agentInNodeList)
        {
            if (!testedShopper.idleBehaviorEntity.wantToExit && testedShopper.transform)
            {
                Vector3 testVector = Vector3.Normalize(testedShopper.transform.position - shopperEntity.transform.position);
                float testDotProduct = Vector3.Dot(normalizedShopperVector, testVector);

                if (testDotProduct > cosAngle)
                {
                    //Debug.Log("Agent detected: " + testedShopper.name);
                    shopperEntity.actionBehaviorEntity.shopperToSteal = testedShopper;
                    shopperEntity.navigationEntity.TargetPosition = shopperEntity.actionBehaviorEntity.shopperToSteal.transform.position;
                    status = Status.Success;
                    return;
                }
            }
        }

        status = Status.Failure;
        
    }
}
