﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsStoppingAlert : ConditionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
		ShopperGlobal global = self.GetComponent<ShopperGlobal>();

        if (Time.time > global.alertBehaviorEntity.timeOnStopAlert || (!global.actionBehaviorEntity.fightArea && global.alertBehaviorEntity.timeOnStopAlert > 10000000000000f))
            status = Status.Success;
        else
            status = Status.Failure;
    }
}