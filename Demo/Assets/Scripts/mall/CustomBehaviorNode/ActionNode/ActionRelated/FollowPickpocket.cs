﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class FollowPickpocket : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        if (shopperEntity.actionBehaviorEntity.shopperToChase != null)
        {
            shopperEntity.navigationEntity.TargetPosition = shopperEntity.actionBehaviorEntity.shopperToChase.transform.position;
            //shopperEntity.navigationEntity.speed *= 1.5f;
            float distanceToTargetShopper = Vector3.Distance(shopperEntity.transform.position, shopperEntity.actionBehaviorEntity.shopperToChase.transform.position);

            //Debug.Log("Distance: " + distanceToTargetShopper);

            // Disable RVO when getting close in order to avoid RVO preventing collision
            if (distanceToTargetShopper < 2.5)
                shopperEntity.navigationEntity.disableRVO = true;

            if (distanceToTargetShopper > 1.8)
            {
                Vector3 pos = shopperEntity.transform.position;
                shopperEntity.navigationEntity.Move();

                status = Status.Running;
                return;
            }

            shopperEntity.navigationEntity.disableRVO = false;
            //shopperEntity.actionBehaviorEntity.shopperToSteal = null;

            /*
             * Starting Fight Part
             */

            /*
             * CHANGING STATE FOR PICKPOCKET
             */
            shopperEntity.actionBehaviorEntity.shopperToChase.SetAnimToFight();
            shopperEntity.actionBehaviorEntity.shopperToChase.behaviorEntity.state = ShopperBehavior.State.FIGHT_BAD;
            shopperEntity.actionBehaviorEntity.shopperToChase.actionBehaviorEntity.shopperToSteal = null;

            // Will be used later for reorientation when going back to idle state
            shopperEntity.actionBehaviorEntity.shopperToChase.idleBehaviorEntity.lastNodeInIdleState = shopperEntity.idleBehaviorEntity.currentNode;
            
            // Change color depending on state
            Renderer[] rendererList = shopperEntity.actionBehaviorEntity.shopperToChase.GetComponentsInChildren<Renderer>();
            foreach (Renderer render in rendererList)
                    render.material = GlobalEntities.stateMaterials[(int)ShopperBehavior.State.FIGHT_BAD];


            /*
             * CHANGING STATE FOR SUPERHERO
             */
            shopperEntity.SetAnimToFight();
            shopperEntity.behaviorEntity.state = ShopperBehavior.State.FIGHT_GOOD;

            // Will be used later for reorientation when going back to idle state
            shopperEntity.idleBehaviorEntity.lastNodeInIdleState = shopperEntity.idleBehaviorEntity.currentNode;

            // Change color depending on state
            rendererList = self.GetComponentsInChildren<Renderer>();
            foreach (Renderer render in rendererList)
                render.material = GlobalEntities.stateMaterials[(int)ShopperBehavior.State.FIGHT_GOOD];



            GlobalEntities.areaHandler.createArea(self.transform.position, Area.AreaType.FIGHT, 1000000000000000000000000f, 7, shopperEntity);

        }
        shopperEntity.actionBehaviorEntity.shopperToChase = null;

        status = Status.Success;
    }
}
