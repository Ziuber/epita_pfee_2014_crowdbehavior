﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectEntity : MonoBehaviour {

    public enum Entity
    {
        AVERAGE,
        PICKPOCKET,
        SUPERHERO
    }

    public Toggle toggleNeigh1;
    public Toggle toggleNeigh2;
    public ShopperGenerator generator;
    public Entity entity;

    public Slider moveSliderMin;
    public Slider moveSliderMax;

    public Slider strSliderMin;
    public Slider strSliderMax;

    public Slider detectSliderMin;
    public Slider detectSliderMax;

    public Slider eagerSliderMin;
    public Slider eagerSliderMax;

    void Start()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            ShopperTemplate entityTemplate = (generator.Templates[(int)(entity)]).GetComponent<ShopperTemplate>();

            ChangeEntityForSlider(entityTemplate);
        }
    }

    public void ChangeValue(bool isOn)
    {
        if (isOn)
        {
            toggleNeigh1.interactable = true;
            toggleNeigh2.interactable = true;
            toggleNeigh1.isOn = false;
            toggleNeigh2.isOn = false;
            this.GetComponent<Toggle>().interactable = false;
        }
    }

    public void SelectNewEntity(bool isOn)
    {
        if (isOn)
        {
            ShopperTemplate entityTemplate = (generator.Templates[(int)(entity)]).GetComponent<ShopperTemplate>();

            if (entity == Entity.AVERAGE)
                Debug.Log("SHOP_AVER");
            if (entity == Entity.PICKPOCKET)
                Debug.Log("SHOP PICKPOCKET");
            if (entity == Entity.SUPERHERO)
                Debug.Log("SHOP SUPERHERO");

            moveSliderMin.interactable = false;
            moveSliderMax.interactable = false;
            strSliderMin.interactable = false;
            strSliderMax.interactable = false;
            detectSliderMin.interactable = false;
            detectSliderMax.interactable = false;
            eagerSliderMin.interactable = false;
            eagerSliderMax.interactable = false;


            ChangeEntityForSlider(entityTemplate);

            moveSliderMin.interactable = true;
            moveSliderMax.interactable = true;
            strSliderMin.interactable = true;
            strSliderMax.interactable = true;
            detectSliderMin.interactable = true;
            detectSliderMax.interactable = true;
            eagerSliderMin.interactable = true;
            eagerSliderMax.interactable = true;
        }
    }

    public void ChangeEntityForSlider(ShopperTemplate entityTemplate)
    {
        moveSliderMin.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;
        moveSliderMax.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;

        strSliderMin.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;
        strSliderMax.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;

        detectSliderMin.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;
        detectSliderMax.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;

        eagerSliderMin.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;
        eagerSliderMax.GetComponent<MinMaxSlider>().selectedTemplate = entityTemplate;

        moveSliderMin.value = entityTemplate.SpeedDistribution.MinValue * 100;
        moveSliderMax.value = entityTemplate.SpeedDistribution.MaxValue * 100;

        strSliderMin.value = entityTemplate.StrengthDistribution.MinValue * 100;
        strSliderMax.value = entityTemplate.StrengthDistribution.MaxValue * 100;

        detectSliderMin.value = entityTemplate.AwarenessDistribution.MinValue * 100;
        detectSliderMax.value = entityTemplate.AwarenessDistribution.MaxValue * 100;

        eagerSliderMin.value = entityTemplate.EagerDistribution.MinValue * 100;
        eagerSliderMax.value = entityTemplate.EagerDistribution.MaxValue * 100;
    }
}
