﻿
using UnityEngine;
using System.Collections;

public class ShopperAction : MonoBehaviour
{
	public ShopperGlobal shopperToSteal = null;
	public ShopperGlobal shopperToChase = null;
	public Area fightArea = null;
	public float fightThreshold = 0f;
}
