﻿using UnityEngine;
using System.Collections;

public class ShopperCoroutine : MonoBehaviour {

    bool fading = false;

    public void FadeOut(float duration)
    {
        StartCoroutine("Fade", -duration);
    }

    public void FadeIn(float duration)
    {
        StartCoroutine("Fade", duration);
    }

    public void FadeOutThenIn(float duration)
    {
        StartCoroutine("FadeOutIn", duration);
    }

    public void FreezePosition(Vector3 freezedPosition, float duration, int agentId)
    {
        StartCoroutine(FreezePos(freezedPosition, duration, agentId));
    }

    IEnumerator FreezePos(Vector3 freezedPosition, float duration, int agentId)
    {
        while (duration > 0f)
        {
			// Should disable position update instead, this will probably make the shopper twitch
            this.transform.position = freezedPosition;
            duration -= Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator FadeOutIn(float duration)
    {
        yield return StartCoroutine("Fade", -duration);
        yield return new WaitForSeconds(1f);
        StartCoroutine("Fade", duration);
    }

    IEnumerator Fade(float fadingDuration)
    {
        bool fadingOut = (fadingDuration < 0.0f);
        float fadingOutSpeed = 1.0f / fadingDuration;

        // grab all child objects
        Renderer[] rendererObjects = this.gameObject.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < rendererObjects.Length; i++)
        {
            rendererObjects[i].enabled = true;
        }

        float currentAlphaValue = 0f;
        
        if (fadingOut)
            currentAlphaValue = 1f;

        // iterate to change alpha value 
        while ((currentAlphaValue >= 0.0f && fadingOut) || (currentAlphaValue <= 1.0f && !fadingOut))
        {
            currentAlphaValue += Time.deltaTime * fadingOutSpeed;
            for (int i = 0; i < rendererObjects.Length; i++)
            {
                Color newColor = rendererObjects[i].material.color;

                newColor.a = currentAlphaValue;
                newColor.a = Mathf.Clamp(newColor.a, 0.0f, 1.0f);
                rendererObjects[i].material.color = newColor;
            }

            yield return null;
        }

        // turn objects off after fading out
        if (fadingOut)
        {
            for (int i = 0; i < rendererObjects.Length; i++)
            {
                rendererObjects[i].enabled = false;
            }
        }
    }
}
