﻿using UnityEngine;
using System.Collections;

public class CustomMenu : MonoBehaviour {
	float currentSliderValue = 0.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI()
	{
		// Reset Button
		
		if (GUI.Button(new Rect(10, 10, 100, 50), "Reset"))
			Application.LoadLevel(0);
		currentSliderValue = GUI.HorizontalSlider (new Rect (10, 70, 100, 50), currentSliderValue, 0.0f, 10.0f);
	}
}
