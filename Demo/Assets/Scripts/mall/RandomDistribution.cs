﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RandomDistribution : System.Object
{
		public AnimationCurve CumulativeDistribution;
		public float MinValue = 0;
		public float MaxValue = 1;

		public float Evaluate ()
		{
				return CumulativeDistribution.Evaluate (Random.value) * (MaxValue - MinValue) + MinValue;
		}
}
